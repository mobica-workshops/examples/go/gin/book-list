package cmd

import (
	"gitlab.com/mobica-workshops/examples/go/gin/book-list/app"
	"gitlab.com/mobica-workshops/examples/go/gin/book-list/config"

	"github.com/spf13/cobra"
)

var port string  //set port
var bind string  //set bind
var migrate bool //set migrate
var load bool    //set load
var wait int     //set wait
var cors bool    //set cors

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Book listing app",
	Long:  "Book listing app",
	Run: func(cmd *cobra.Command, args []string) {
		config, err := config.GetConfiguration()
		if err == nil {
			config.Server.Host = bind
			config.Server.Port = port
			app.RunApp(config, load)
		}
	},
}

func init() {
	rootCmd.AddCommand(serveCmd)

	serveCmd.PersistentFlags().StringVarP(&bind, "bind", "b", "127.0.0.1", "This flag sets the IP to bind for our API server - overwrites config")
	serveCmd.PersistentFlags().StringVarP(&port, "port", "p", "8080", "This flag sets the port of our API server - overwrites config")
	serveCmd.PersistentFlags().BoolVarP(&migrate, "migrate", "m", false, "This decide if we should migrate or not when starting")
	serveCmd.PersistentFlags().BoolVarP(&load, "load", "l", false, "This decide if we should load test data or not when starting")
	serveCmd.PersistentFlags().IntVarP(&wait, "wait", "w", 0, "This flag sets how many seconds to wait for the database - default 0 sec")
	serveCmd.PersistentFlags().BoolVarP(&cors, "cors", "c", false, "This decide if we should enable cors - in the Prod NGiNX is solving cors for us")
}
