/*
Copyright © 2022 Arkadiusz Tułodziecki <atulodzi@gmail.com>
*/

package cmd

import (
	"log"
	"os"
	"path"
	"path/filepath"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "book-list",
	Short: "Book listing app",
	Long:  "Book listing app",
}

func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is ./secrets/local.env.yaml)")
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func initConfig() {
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		ex, err := os.Executable()
		cobra.CheckErr(err)
		executablePath := filepath.Dir(ex)

		viper.AddConfigPath(path.Join(executablePath, "secrets"))
		viper.SetConfigName("local.env")
	}

	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		log.Panic("reading config failed")
	}
}
