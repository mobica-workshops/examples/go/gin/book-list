FROM golang:latest AS builder
WORKDIR /go/src/gitlab.com/mobica-workshops/examples/go/gin/book-list
ADD . ./
RUN ./build.sh linux

FROM alpine:latest
RUN mkdir -p "/var/application"
COPY public /var/application/public
COPY --from=builder /go/src/gitlab.com/mobica-workshops/examples/go/gin/book-list/book-list /bin/book-list
EXPOSE 8080
ENTRYPOINT ["/bin/book-list"]
CMD ["serve", "--config", "/secrets/local.env.yaml", "-b", "0.0.0.0", "-p", "8080", "-m"]
