FROM alpine:latest

RUN mkdir -p "/var/application"
COPY public /var/application/public
COPY book-list /bin/book-list

EXPOSE 8080

ENTRYPOINT ["/bin/book-list"]

CMD ["serve", "--config", "/secrets/local.env.yaml", "-b", "0.0.0.0", "-p", "8080", "-m"]