#!/usr/bin/env bash
export GIN_MODE=test
set -e
CFG_FILE="$(pwd)/secrets/local.env.yaml" go test -v ./... --cover
