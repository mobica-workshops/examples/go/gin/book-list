package app

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"net/http"

	"gitlab.com/mobica-workshops/examples/go/gin/book-list/config"
	"gitlab.com/mobica-workshops/examples/go/gin/book-list/src/db"
	"gitlab.com/mobica-workshops/examples/go/gin/book-list/src/helpers/validators"
	"gitlab.com/mobica-workshops/examples/go/gin/book-list/src/middleware"
	"gitlab.com/mobica-workshops/examples/go/gin/book-list/src/routes"
	"gitlab.com/mobica-workshops/examples/go/gin/book-list/src/services"
	"gitlab.com/mobica-workshops/examples/go/gin/book-list/src/support"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
	"gorm.io/gorm"
)

type App struct {
	BookService   *services.Service
	HealthService *services.Service
	router        *gin.Engine
	Database      *gorm.DB
}

func RunApp(config *config.Config, seed bool) {
	if seed {
		support.Seed(config)
	}

	startServer(config)
}

func stopIfNeed(app *App) {
	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal, 1)
	// kill (no param) default send syscall.SIGTERM
	// kill -2 is syscall.SIGINT
	// kill -9 is syscall.SIGKILL but can't be catch, so don't need add it
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	log.Println("Shutting down server...")
	<-quit
	log.Println("Shutting down server...")

	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	_, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	// if err := app.router.Shutdown(ctx); err != nil {
	// 	log.Fatal("Server forced to shutdown: ", err)
	// }
	<-quit
	log.Println("Server exiting")
}

func startServer(config *config.Config) {
	app := App{}
	app.initiateServer(config)
	app.registerAPI()
	defer app.closeServer()

	app.runServer(config.Server.Port)
	stopIfNeed(&app)
}

func (app *App) initiateServer(config *config.Config) {
	app.Database = db.ConnectDB(config.DBConnection.Url)
	app.BookService = services.NewBookService(app.Database)
	app.HealthService = services.NewHealthService(app.Database)
	app.router = gin.Default()
}

func (app *App) registerAPI() {
	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
		v.RegisterValidation("ImgType", validators.ImageTypeValidator)
	}

	app.router.Use(middleware.CORSMiddleware())
	mainRoute := app.router.Group("/v1")
	routes.AddBookRoutes(mainRoute, app.BookService)
	routes.AddHealthRoutes(mainRoute, app.HealthService)
}

func (app *App) runServer(port string) {

	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", port),
		Handler: app.router,
	}

	// Initializing the server in a goroutine so that
	// it won't block the graceful shutdown handling below
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal, 1)
	// kill (no param) default send syscall.SIGTERM
	// kill -2 is syscall.SIGINT
	// kill -9 is syscall.SIGKILL but can't be catch, so don't need add it
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shutting down server...")

	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown: ", err)
		os.Exit(1)
	}

	log.Println("Server exiting")
	os.Exit(0)
}

func (app *App) closeServer() {
	db.DisconnectDB(app.Database)
}
