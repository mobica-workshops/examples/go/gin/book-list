#!/usr/bin/env bash
set -e

# import code quality functions
source $(dirname "$0")/scripts/code_quality.sh

# Run staticcheck
static_check_results=$(run_staticcheck)
# Print formatted output
echo "$static_check_results"

# Run code style check
code_style_results=$(run_codestyle)
# Print formatted output
echo "$code_style_results"

# Check if checks return successfull process.
if [[ $code_style_results == *"STATUS - OK"* && $static_check_results == *"STATUS - OK"* ]]; then
    exit 0
else
    exit 1
fi