package repository

import (
	"gorm.io/gorm"
)

type Repository struct {
	db *gorm.DB
}

type RequestError struct {
	Error   error
	Message string
	Code    int
}
