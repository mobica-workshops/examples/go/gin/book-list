package repository

import (
	"errors"
	"log"
	"net/http"
	"reflect"

	"gitlab.com/mobica-workshops/examples/go/gin/book-list/src/models"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type BookRepository interface {
	FindOne(id uint) (*models.Book, *RequestError)
	GetBatch(input *models.PaginationInput) (*[]models.Book, int64, *RequestError)
	Create(input *models.Book) (*models.Book, *RequestError)
	Update(id uint, input map[string]interface{}) (*models.Book, *RequestError)
	Delete(id uint) *RequestError
	HealthCheck() bool
}

func CreateBookRepository(db *gorm.DB) BookRepository {
	return &Repository{db}
}

func (r *Repository) FindOne(id uint) (*models.Book, *RequestError) {
	var entity models.Book

	result := r.db.Preload(clause.Associations).Find(&entity, id)

	if result.Error != nil {
		log.Fatal(result.Error)
		return nil, &RequestError{Error: result.Error, Message: "Could not execute db query", Code: http.StatusInternalServerError}
	} else if result.RowsAffected < 1 {
		return nil, &RequestError{Error: result.Error, Message: "Record not found", Code: http.StatusNotFound}
	}

	return &entity, nil
}

func (r *Repository) GetBatch(input *models.PaginationInput) (*[]models.Book, int64, *RequestError) {
	var batch []models.Book
	var count int64
	result := r.db.Limit(input.Limit).Offset(input.Offset).Preload(clause.Associations).Find(&batch)
	r.db.Model(&models.Book{}).Count(&count)

	if result.Error != nil {
		log.Fatal(result.Error)
		return nil, 0, &RequestError{Error: result.Error, Message: "Could not execute db query", Code: http.StatusInternalServerError}
	}

	return &batch, count, nil
}

func (r *Repository) Create(input *models.Book) (*models.Book, *RequestError) {
	if err := r.db.Create(input).Error; err != nil {
		return nil, &RequestError{Error: err, Message: "Could not execute db query", Code: http.StatusInternalServerError}
	}

	return input, nil
}

func (r *Repository) Update(id uint, input map[string]interface{}) (*models.Book, *RequestError) {
	book, err := r.FindOne(id)
	if err != nil {
		return nil, err
	}

	book.AssignUpdate(input)

	if val, ok := input["image"]; ok {
		imgType, imgTypeOk := val.(map[string]interface{})["type"]
		data, dataOk := val.(map[string]interface{})["data"]

		if len(val.(map[string]interface{})) == 0 {
			if !reflect.ValueOf(book.Image).IsZero() {
				result := r.db.Unscoped().Delete(models.Image{}, book.Image.ID)

				if result.Error != nil {
					return nil, &RequestError{Error: result.Error, Message: "Could not execute db query", Code: http.StatusInternalServerError}
				}

				book.Image = models.Image{}
			}
		} else if reflect.ValueOf(book.Image).IsZero() {
			if imgTypeOk && (imgType == "url" || imgType == "base64") && dataOk {
				book.Image = models.Image{Type: imgType.(string), Data: data.(string)}
			} else {
				return nil, &RequestError{Error: errors.New("could not update book"), Message: "Could not udate book, missing image type or data", Code: http.StatusBadRequest}
			}
		} else {
			if imgTypeOk && (imgType == "url" || imgType == "base64") {
				book.Image.Type = imgType.(string)
			}

			if dataOk {
				book.Image.Data = data.(string)
			}
		}
	}

	if err := r.db.Session(&gorm.Session{FullSaveAssociations: true}).Updates(&book).Error; err != nil {
		return nil, &RequestError{Error: err, Message: "Could not execute db query", Code: http.StatusInternalServerError}
	}

	return book, nil
}

func (r *Repository) Delete(id uint) *RequestError {
	entity := models.Book{}

	result := r.db.Unscoped().Delete(entity, id)

	if result.Error != nil {
		return &RequestError{Error: result.Error, Message: "Could not execute db query", Code: http.StatusInternalServerError}
	} else if result.RowsAffected < 1 {
		return &RequestError{Error: result.Error, Message: "Record not found", Code: http.StatusNotFound}
	}

	return nil
}

func (r *Repository) HealthCheck() bool {
	pg := true

	sqlDB, err := r.db.DB()

	if err != nil {
		pg = false
	}

	if err := sqlDB.Ping(); err != nil {
		pg = false
	}

	return pg
}
