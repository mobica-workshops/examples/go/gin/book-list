package repository

import (
	"errors"
	"regexp"
	"testing"

	"gitlab.com/mobica-workshops/examples/go/gin/book-list/helpers"
	"gitlab.com/mobica-workshops/examples/go/gin/book-list/src/db"
	"gitlab.com/mobica-workshops/examples/go/gin/book-list/src/models"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/require"
)

func TestCreateBookRepository(t *testing.T) {
	gormDB, _ := db.GetTestDB()
	repo := CreateBookRepository(gormDB)
	require.NotEmpty(t, repo)
}

func TestFindOne(t *testing.T) {
	gormDB, mock := db.GetTestDB()
	r := Repository{gormDB}

	mock.ExpectQuery(
		regexp.QuoteMeta("SELECT * FROM `books`")).WithArgs(1).WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))
	mock.ExpectQuery(
		regexp.QuoteMeta("SELECT * FROM `images`")).WithArgs(1).WillReturnRows(sqlmock.NewRows([]string{"id"}))

	_, error := r.FindOne(1)

	require.Empty(t, error)
	err := mock.ExpectationsWereMet()
	require.NoError(t, err)

}

func TestFindOneNotFound(t *testing.T) {
	gormDB, mock := db.GetTestDB()
	r := Repository{gormDB}

	mock.ExpectQuery(
		regexp.QuoteMeta("SELECT * FROM `books`")).WithArgs(1).WillReturnRows(sqlmock.NewRows([]string{}))

	_, error := r.FindOne(1)

	require.Equal(t, error.Code, 404)
	err := mock.ExpectationsWereMet()
	require.NoError(t, err)
}

func TestGetBatch(t *testing.T) {
	gormDB, mock := db.GetTestDB()
	r := Repository{gormDB}

	mock.ExpectQuery(
		regexp.QuoteMeta("SELECT * FROM `books`")).WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))
	mock.ExpectQuery(
		regexp.QuoteMeta("SELECT * FROM `images`")).WithArgs(1).WillReturnRows(sqlmock.NewRows([]string{"id"}))
	mock.ExpectQuery(
		regexp.QuoteMeta("SELECT count(*) FROM `books`")).WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(1))

	input := models.PaginationInput{Limit: 1, Offset: 1}
	_, _, error := r.GetBatch(&input)

	require.Empty(t, error)
	err := mock.ExpectationsWereMet()
	require.NoError(t, err)
}

func TestCreate(t *testing.T) {
	gormDB, mock := db.GetTestDB()
	r := Repository{gormDB}

	mock.ExpectBegin()
	mock.ExpectExec(regexp.QuoteMeta("INSERT INTO `books`")).
		WithArgs(helpers.AnyTime{}, helpers.AnyTime{}, sqlmock.AnyArg(), "Test", "string", "string", "string").
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	input := models.Book{Title: "Test", Author: "string", Year: "string", Description: "string"}
	_, err := r.Create(&input)

	require.Empty(t, err)
	error := mock.ExpectationsWereMet()
	require.NoError(t, error)
}

func TestCreateError(t *testing.T) {
	gormDB, mock := db.GetTestDB()
	r := Repository{gormDB}

	mock.ExpectBegin()
	mock.ExpectExec(regexp.QuoteMeta("INSERT INTO `books`")).
		WithArgs(helpers.AnyTime{}, helpers.AnyTime{}, sqlmock.AnyArg(), "Test", "string", "string", "string").
		WillReturnError(errors.New("Error"))
	mock.ExpectRollback()

	input := models.Book{Title: "Test", Author: "string", Year: "string", Description: "string"}
	_, err := r.Create(&input)

	require.Equal(t, err.Code, 500)
	error := mock.ExpectationsWereMet()
	require.NoError(t, error)
}

func TestUpdate(t *testing.T) {
	gormDB, mock := db.GetTestDB()
	r := Repository{gormDB}

	mock.ExpectQuery(
		regexp.QuoteMeta("SELECT * FROM `books`")).WillReturnRows(sqlmock.NewRows([]string{"ID"}).AddRow(1))
	mock.ExpectQuery(
		regexp.QuoteMeta("SELECT * FROM `images`")).WithArgs(1).WillReturnRows(sqlmock.NewRows([]string{"id"}))
	mock.ExpectBegin()
	mock.ExpectExec(regexp.QuoteMeta("UPDATE `books`")).
		WithArgs(helpers.AnyTime{}, "Test", "string", "string", "string", 1).
		WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectCommit()

	input := map[string]interface{}{"title": "Test", "author": "string", "year": "string", "description": "string"}
	_, err := r.Update(1, input)

	require.Empty(t, err)
	error := mock.ExpectationsWereMet()
	require.NoError(t, error)
}

func TestUpdateNoRecord(t *testing.T) {
	gormDB, mock := db.GetTestDB()
	r := Repository{gormDB}

	mock.ExpectQuery(
		regexp.QuoteMeta("SELECT * FROM `books`")).WillReturnRows(sqlmock.NewRows([]string{}))

	input := map[string]interface{}{"title": "Test", "author": "string", "year": "string", "description": "string"}
	_, err := r.Update(1, input)

	require.Equal(t, err.Code, 404)
	error := mock.ExpectationsWereMet()
	require.NoError(t, error)
}

func TestUpdateError(t *testing.T) {
	gormDB, mock := db.GetTestDB()
	r := Repository{gormDB}

	mock.ExpectQuery(
		regexp.QuoteMeta("SELECT * FROM `books`")).WillReturnRows(sqlmock.NewRows([]string{"ID"}).AddRow(1))
	mock.ExpectQuery(
		regexp.QuoteMeta("SELECT * FROM `images`")).WithArgs(1).WillReturnRows(sqlmock.NewRows([]string{"id"}))
	mock.ExpectBegin()
	mock.ExpectExec(regexp.QuoteMeta("UPDATE `books`")).
		WithArgs(helpers.AnyTime{}, "Test", "string", "string", "string", 1).
		WillReturnError(errors.New("Error"))
	mock.ExpectRollback()

	input := map[string]interface{}{"title": "Test", "author": "string", "year": "string", "description": "string"}
	_, err := r.Update(1, input)

	require.Equal(t, err.Code, 500)
	error := mock.ExpectationsWereMet()
	require.NoError(t, error)
}

func TestDelete(t *testing.T) {
	gormDB, mock := db.GetTestDB()
	r := Repository{gormDB}

	mock.ExpectBegin()
	mock.ExpectExec("DELETE FROM `books`").WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectCommit()

	err := r.Delete(1)

	require.Empty(t, err)
	error := mock.ExpectationsWereMet()
	require.NoError(t, error)
}

func TestDeleteNotFound(t *testing.T) {
	gormDB, mock := db.GetTestDB()
	r := Repository{gormDB}

	mock.ExpectBegin()
	mock.ExpectExec("DELETE FROM `books`").WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()

	err := r.Delete(1)

	require.Equal(t, err.Code, 404)
	error := mock.ExpectationsWereMet()
	require.NoError(t, error)
}

func TestDeleteError(t *testing.T) {
	gormDB, mock := db.GetTestDB()
	r := Repository{gormDB}

	mock.ExpectBegin()
	mock.ExpectExec("DELETE FROM `books`").WillReturnError(errors.New("Error"))
	mock.ExpectRollback()

	err := r.Delete(1)

	require.Equal(t, err.Code, 500)
	error := mock.ExpectationsWereMet()
	require.NoError(t, error)
}

func TestHealthCheck(t *testing.T) {
	gormDB, _ := db.GetTestDB()
	r := Repository{gormDB}

	response := r.HealthCheck()
	require.Equal(t, response, true)
}
