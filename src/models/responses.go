package models

type Response struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Content interface{} `json:"content"`
}

type ErrorResponse struct {
	Status  int      `json:"status"`
	Message string   `json:"message"`
	Errors  []string `json:"errors"`
}

type Pagination struct {
	Count   int64       `json:"count"`
	Skip    int         `json:"skip"`
	Limit   int         `json:"limit"`
	Results interface{} `json:"results"`
}

type HealthResponse struct {
	Alive    bool `json:"alive"`
	Postgres bool `json:"postgres"`
}

type NewBookResponse struct {
	ID uint `json:"id"`
}

func GenerateResponse(status int, message string, content interface{}) *Response {
	return &Response{
		Status:  status,
		Message: message,
		Content: content,
	}
}

func GenerateErrorResponse(status int, message string, errorMessages []string) *ErrorResponse {
	return &ErrorResponse{
		Status:  status,
		Message: message,
		Errors: errorMessages,
	}
}
