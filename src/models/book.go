package models

import 	"gorm.io/gorm"

type Book struct {
	gorm.Model  `json:"-"`
	ID          uint   `json:"id" gorm:"primaryKey"`
	Title       string `json:"title" binding:"required"`
	Author      string `json:"author" binding:"required"`
	Year        string `json:"year" binding:"required" time_format:"2006"`
	Description string `json:"description" binding:"omitempty"`
	Image       Image  `json:"image,omitempty" form:"image" binding:"omitempty,dive" gorm:"constraint:OnDelete:CASCADE;"`
}

type Image struct {
	gorm.Model `json:"-"`
	ID         uint   `json:"-" gorm:"primaryKey"`
	BookID     uint   `json:"-" gorm:"foreignKey:id"`
	Type       string `json:"type,omitempty" binding:"omitempty,ImgType"`
	Data       string `json:"data,omitempty" binding:"omitempty"`
}

func (b *Book) AssignUpdate(update map[string]interface{}) {
	if val, ok := update["title"]; ok && val != "" {
		b.Title = val.(string)
	}

	if val, ok := update["author"]; ok && val != "" {
		b.Author = val.(string)
	}

	if val, ok := update["year"]; ok && val != "" {
		b.Year = val.(string)
	}

	if val, ok := update["description"]; ok {
		b.Description = val.(string)
	}
}
