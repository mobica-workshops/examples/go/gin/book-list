package models

type PaginationInput struct {
	Limit  int `form:"limit,default=10" binding:"gte=1"`
	Offset int `form:"skip,default=0"`
}

type BookID struct {
	ID uint `uri:"id" binding:"required,numeric,gt=0"`
}
