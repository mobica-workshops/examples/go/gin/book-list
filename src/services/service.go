package services

import (
	"gitlab.com/mobica-workshops/examples/go/gin/book-list/src/repository"
)

type Service struct {
	repository repository.BookRepository
}
