package services

import (
	"gitlab.com/mobica-workshops/examples/go/gin/book-list/src/models"
	"gitlab.com/mobica-workshops/examples/go/gin/book-list/src/repository"

	"github.com/stretchr/testify/mock"
)

type MockedRepository struct {
	mock.Mock
}

func (r *MockedRepository) FindOne(id uint) (*models.Book, *repository.RequestError) {
	args := r.Called(id)
	return args.Get(0).(*models.Book), args.Get(1).(*repository.RequestError)
}

func (r *MockedRepository) GetBatch(input *models.PaginationInput) (*[]models.Book, int64, *repository.RequestError) {
	args := r.Called(input)
	return args.Get(0).(*[]models.Book), args.Get(1).(int64), args.Get(2).(*repository.RequestError)
}

func (r *MockedRepository) Create(input *models.Book) (*models.Book, *repository.RequestError) {
	args := r.Called(input)
	return args.Get(0).(*models.Book), args.Get(1).(*repository.RequestError)
}

func (r *MockedRepository) Update(id uint, input map[string]interface{}) (*models.Book, *repository.RequestError) {
	args := r.Called(id, input)
	return args.Get(0).(*models.Book), args.Get(1).(*repository.RequestError)
}

func (r *MockedRepository) Delete(id uint) *repository.RequestError {
	args := r.Called(id)
	return args.Get(0).(*repository.RequestError)
}

func (r *MockedRepository) HealthCheck() bool {
	args := r.Called()
	return args.Bool(0)
}
