package services

import (
	"fmt"
	"net/http"

	"gitlab.com/mobica-workshops/examples/go/gin/book-list/src/models"
	"gitlab.com/mobica-workshops/examples/go/gin/book-list/src/repository"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type BookService interface {
	CreateBook(c *gin.Context)
	UpdateBook(c *gin.Context)
	DeleteBook(c *gin.Context)
	GetBooks(c *gin.Context)
	GetBook(c *gin.Context)
}

func NewBookService(db *gorm.DB) *Service {
	repo := repository.CreateBookRepository(db)
	return &Service{repo}
}

func (s *Service) CreateBook(c *gin.Context) {
	input := models.Book{}

	if c.BindJSON(&input) != nil {
		c.JSON(http.StatusBadRequest, models.GenerateErrorResponse(http.StatusBadRequest, "Problem with adding book", []string{"Provide relevant fields"}))
		c.Abort()

		return
	}

	newBook, err := s.repository.Create(&input)

	if err != nil {
		c.JSON(err.Code, models.GenerateErrorResponse(err.Code, "Problem with adding book", []string{err.Message}))
		c.Abort()
		return
	}

	c.JSON(http.StatusCreated, models.GenerateResponse(http.StatusCreated, fmt.Sprintf("book: %d created", newBook.ID), &models.NewBookResponse{ID: newBook.ID}))
}

func (s *Service) UpdateBook(c *gin.Context) {
	urlParam := models.BookID{}
	input := map[string]interface{}{}

	if c.ShouldBindUri(&urlParam) != nil {
		c.JSON(http.StatusBadRequest, models.GenerateErrorResponse(http.StatusBadRequest, "Problem with updating book", []string{"Provide existing book id"}))
		c.Abort()

		return
	}

	if c.BindJSON(&input) != nil {
		c.JSON(http.StatusBadRequest, models.GenerateErrorResponse(http.StatusBadRequest, "Problem with updating book", []string{"Provide relevant fields"}))
		c.Abort()

		return
	}

	updatedBook, err := s.repository.Update(urlParam.ID, input)

	if err != nil {
		c.JSON(err.Code, models.GenerateErrorResponse(err.Code, "Problem with updating book", []string{err.Message}))
		c.Abort()
		return
	}

	c.JSON(http.StatusAccepted, models.GenerateResponse(http.StatusAccepted, fmt.Sprintf("book: %d updated", updatedBook.ID), updatedBook))
}

func (s *Service) DeleteBook(c *gin.Context) {
	urlParam := models.BookID{}

	if c.ShouldBindUri(&urlParam) != nil {
		c.JSON(http.StatusBadRequest, models.GenerateErrorResponse(http.StatusBadRequest, "Problem with updating book", []string{"Provide existing book id"}))
		c.Abort()

		return
	}

	if err := s.repository.Delete(urlParam.ID); err != nil {
		c.JSON(err.Code, models.GenerateErrorResponse(err.Code, "Problem with updating book", []string{err.Message}))
		c.Abort()

		return
	}

	c.JSON(http.StatusAccepted, models.GenerateResponse(http.StatusAccepted, "successfully deleted", nil))
}

func (s *Service) GetBooks(c *gin.Context) {
	pagination := models.PaginationInput{}

	if c.ShouldBindQuery(&pagination) != nil {
		c.JSON(http.StatusNotFound, models.GenerateErrorResponse(http.StatusNotFound, "Problem with updating book", []string{"Provide relevant pagination fields"}))
		c.Abort()

		return
	}

	books, count, err := s.repository.GetBatch(&pagination)

	if err != nil {
		c.JSON(err.Code, models.GenerateErrorResponse(err.Code, "Problem with finding books", []string{err.Message}))
		c.Abort()

		return
	}

	c.JSON(http.StatusOK, models.GenerateResponse(http.StatusOK, fmt.Sprintf("books - skip: %d; limit: %d", pagination.Offset, pagination.Limit), &models.Pagination{Count: count, Skip: pagination.Offset, Limit: pagination.Limit, Results: &books}))
}

func (s *Service) GetBook(c *gin.Context) {
	urlParam := models.BookID{}

	if c.ShouldBindUri(&urlParam) != nil {
		c.JSON(http.StatusBadRequest, models.GenerateErrorResponse(http.StatusBadRequest, "Problem with finding book", []string{"Provide existing book id"}))
		c.Abort()

		return
	}

	book, err := s.repository.FindOne(urlParam.ID)

	if err != nil {
		c.JSON(http.StatusNotFound, models.GenerateErrorResponse(http.StatusNotFound, "Problem with finding book", []string{err.Message}))
		c.Abort()

		return
	}

	c.JSON(http.StatusOK, models.GenerateResponse(http.StatusOK, fmt.Sprintf("book-list book id:%d found", urlParam.ID), &book))
}
