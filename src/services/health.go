package services

import (
	"net/http"

	"gitlab.com/mobica-workshops/examples/go/gin/book-list/src/models"
	"gitlab.com/mobica-workshops/examples/go/gin/book-list/src/repository"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type HealthService interface {
	CheckHealth(c *gin.Context)
}

func NewHealthService(db *gorm.DB) *Service {
	repo := repository.CreateBookRepository(db)
	return &Service{repo}
}

func (s *Service) CheckHealth(c *gin.Context) {
	dbHealth := s.repository.HealthCheck()

	c.JSON(http.StatusOK, models.GenerateResponse(http.StatusOK, "book-list api health", &models.HealthResponse{Alive: true, Postgres: dbHealth}))
}
