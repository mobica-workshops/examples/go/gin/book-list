package services

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/mobica-workshops/examples/go/gin/book-list/src/db"
	"gitlab.com/mobica-workshops/examples/go/gin/book-list/src/helpers/validators"
	"gitlab.com/mobica-workshops/examples/go/gin/book-list/src/models"
	"gitlab.com/mobica-workshops/examples/go/gin/book-list/src/repository"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var (
	bookUpdateBody = []byte(`{
	"title": "test_title",
	"author": "test test",
	"year": "2002",
	"description": "test",
	"image": {
	  "type": "url",
	  "data": "test"
	}
  }`)
	partialBookUpdateBody = []byte(`{
	"title": "test_title",
	"author": "test test"
  }`)
	id              uint = 1
	bookInput            = models.Book{Title: "test_title", Author: "test test", Year: "2002", Description: "test", Image: models.Image{Type: "url", Data: "test"}}
	updateBookInput      = map[string]interface{}{"author": "test test", "description": "test", "image": map[string]interface{}{"type": "url", "data": "test"}, "title": "test_title", "year": "2002"}
)

func TestNewBookService(t *testing.T) {
	gormDB, _ := db.GetTestDB()
	service := NewBookService(gormDB)

	require.NotEmpty(t, service)
}

func TestCreateBook(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
		v.RegisterValidation("ImgType", validators.ImageTypeValidator)
	}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodPost, "/", bytes.NewBuffer(bookUpdateBody))
	bookOutput := bookInput
	bookOutput.ID = 1

	mockedRepo.On("Create", &bookInput).Return(&bookOutput, (*repository.RequestError)(nil))

	s.CreateBook(c)

	got := models.Response{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.Response{Status: http.StatusCreated, Message: "book: 1 created", Content: map[string]interface{}{"id": float64(1)}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusCreated, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestCreateBookMissingArguments(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodPost, "/", bytes.NewBuffer(partialBookUpdateBody))

	s.CreateBook(c)

	got := models.ErrorResponse{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.ErrorResponse{Status: http.StatusBadRequest, Message: "Problem with adding book", Errors: []string{"Provide relevant fields"}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusBadRequest, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestCreateBookRepositoryError(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodPost, "/", bytes.NewBuffer(bookUpdateBody))
	mockedRepo.On("Create", &bookInput).Return((*models.Book)(nil), &repository.RequestError{Code: http.StatusNotFound, Message: "not found"})

	s.CreateBook(c)

	got := models.ErrorResponse{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.ErrorResponse{Status: http.StatusNotFound, Message: "Problem with adding book", Errors: []string{"not found"}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusNotFound, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestUpdateBook(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodPut, "/", bytes.NewBuffer(bookUpdateBody))
	c.Params = append(c.Params, gin.Param{Key: "id", Value: "1"})
	bookOutput := bookInput
	bookOutput.ID = id
	mockedRepo.On("Update", bookOutput.ID, updateBookInput).Return(&bookOutput, (*repository.RequestError)(nil))

	s.UpdateBook(c)

	got := models.Response{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.Response{Status: http.StatusAccepted, Message: "book: 1 updated", Content: map[string]interface{}{"author": "test test", "description": "test", "id": float64(1), "image": map[string]interface{}{"type": "url", "data": "test"}, "title": "test_title", "year": "2002"}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusAccepted, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestUpdateBookMissingId(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodPut, "/", bytes.NewBuffer(bookUpdateBody))

	s.UpdateBook(c)

	got := models.ErrorResponse{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.ErrorResponse{Status: http.StatusBadRequest, Message: "Problem with updating book", Errors: []string{"Provide existing book id"}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusBadRequest, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestUpdateBookRepositoryError(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodPut, "/", bytes.NewBuffer(bookUpdateBody))
	c.Params = append(c.Params, gin.Param{Key: "id", Value: "1"})
	mockedRepo.On("Update", id, updateBookInput).Return((*models.Book)(nil), &repository.RequestError{Code: http.StatusNotFound, Message: "book with id 1 not found"})

	s.UpdateBook(c)

	got := models.ErrorResponse{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.ErrorResponse{Status: http.StatusNotFound, Message: "Problem with updating book", Errors: []string{"book with id 1 not found"}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusNotFound, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestDeleteBook(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodPost, "/", bytes.NewBuffer([]byte("{}")))
	c.Params = append(c.Params, gin.Param{Key: "id", Value: "1"})
	mockedRepo.On("Delete", id).Return((*repository.RequestError)(nil))

	s.DeleteBook(c)

	got := models.Response{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.Response{Status: http.StatusAccepted, Message: "successfully deleted", Content: interface{}(nil)}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusAccepted, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestDeleteBookMissingId(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodPost, "/", bytes.NewBuffer([]byte("{}")))

	s.DeleteBook(c)

	got := models.ErrorResponse{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.ErrorResponse{Status: http.StatusBadRequest, Message: "Problem with updating book", Errors: []string{"Provide existing book id"}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusBadRequest, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestDeleteBookRepositoryError(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodPost, "/", bytes.NewBuffer(bookUpdateBody))
	c.Params = append(c.Params, gin.Param{Key: "id", Value: "1"})
	mockedRepo.On("Delete", id).Return(&repository.RequestError{Code: http.StatusNotFound, Message: "book with id 1 not found"})

	s.DeleteBook(c)

	got := models.ErrorResponse{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.ErrorResponse{Status: http.StatusNotFound, Message: "Problem with updating book", Errors: []string{"book with id 1 not found"}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusNotFound, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestGetBook(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodGet, "/", bytes.NewBuffer([]byte("{}")))
	c.Params = append(c.Params, gin.Param{Key: "id", Value: "1"})
	bookOutput := bookInput
	bookOutput.ID = id
	mockedRepo.On("FindOne", id).Return(&bookOutput, (*repository.RequestError)(nil))

	s.GetBook(c)

	got := models.Response{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.Response{Status: http.StatusOK, Message: "book-list book id:1 found", Content: map[string]interface{}{"author": "test test", "description": "test", "id": float64(1), "image": map[string]interface{}{"type": "url", "data": "test"}, "title": "test_title", "year": "2002"}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusOK, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestGetBookMissingId(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodPost, "/", bytes.NewBuffer([]byte("{}")))

	s.GetBook(c)

	got := models.ErrorResponse{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.ErrorResponse{Status: http.StatusBadRequest, Message: "Problem with finding book", Errors: []string{"Provide existing book id"}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusBadRequest, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestGetBookRepositoryError(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodPost, "/", bytes.NewBuffer(bookUpdateBody))
	c.Params = append(c.Params, gin.Param{Key: "id", Value: "1"})
	mockedRepo.On("FindOne", id).Return((*models.Book)(nil), &repository.RequestError{Code: http.StatusNotFound, Message: "book with id 1 not found"})

	s.GetBook(c)

	got := models.ErrorResponse{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.ErrorResponse{Status: http.StatusNotFound, Message: "Problem with finding book", Errors: []string{"book with id 1 not found"}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusNotFound, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestGetBooks(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodGet, "/?limit=1&skip=1", bytes.NewBuffer([]byte("{}")))
	pagination := models.PaginationInput{Limit: 1, Offset: 1}
	bookOutput := bookInput
	bookOutput.ID = id
	mockedRepo.On("GetBatch", &pagination).Return(&[]models.Book{bookOutput}, int64(1), (*repository.RequestError)(nil))

	s.GetBooks(c)

	got := models.Response{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.Response{Status: http.StatusOK, Message: "books - skip: 1; limit: 1", Content: map[string]interface{}{"count": float64(1), "limit": float64(1), "results": []interface{}{map[string]interface{}{"author": "test test", "description": "test", "id": float64(1), "image": map[string]interface{}{"type": "url", "data": "test"}, "title": "test_title", "year": "2002"}}, "skip": float64(1)}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusOK, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestGetBooksDefaultPagination(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodGet, "/", bytes.NewBuffer([]byte("{}")))
	pagination := models.PaginationInput{Limit: 10, Offset: 0}
	bookOutput := bookInput
	bookOutput.ID = id
	mockedRepo.On("GetBatch", &pagination).Return(&[]models.Book{bookOutput}, int64(1), (*repository.RequestError)(nil))

	s.GetBooks(c)

	got := models.Response{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.Response{Status: http.StatusOK, Message: "books - skip: 0; limit: 10", Content: map[string]interface{}{"count": float64(1), "limit": float64(10), "results": []interface{}{map[string]interface{}{"author": "test test", "description": "test", "id": float64(1), "image": map[string]interface{}{"type": "url", "data": "test"}, "title": "test_title", "year": "2002"}}, "skip": float64(0)}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusOK, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestGetBooksRepositoryError(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodGet, "/", bytes.NewBuffer(bookUpdateBody))
	pagination := models.PaginationInput{Limit: 10, Offset: 0}
	mockedRepo.On("GetBatch", &pagination).Return((*[]models.Book)(nil), int64(0), &repository.RequestError{Code: http.StatusNotFound, Message: "books not found"})

	s.GetBooks(c)

	got := models.ErrorResponse{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.ErrorResponse{Status: http.StatusNotFound, Message: "Problem with finding books", Errors: []string{"books not found"}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusNotFound, w.Code)
	mockedRepo.AssertExpectations(t)
}
