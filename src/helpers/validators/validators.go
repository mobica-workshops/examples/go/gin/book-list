package validators

import "github.com/go-playground/validator/v10"

var ImageTypeValidator validator.Func = func(fl validator.FieldLevel) bool {
	imageType, ok := fl.Field().Interface().(string)
	if ok {
		if imageType != "url" && imageType != "base64" {
			return false
		}
	}
	return true
}