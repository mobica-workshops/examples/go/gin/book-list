package support

import (
	"log"

	"gitlab.com/mobica-workshops/examples/go/gin/book-list/config"
	"gitlab.com/mobica-workshops/examples/go/gin/book-list/src/db"
	"gitlab.com/mobica-workshops/examples/go/gin/book-list/src/models"
)

func Seed(config *config.Config) {
	database := db.ConnectDB(config.DBConnection.Url)
	log.Println("Seed started")

	defer db.DisconnectDB(database)

	if database.Migrator().DropTable(&models.Book{}, &models.Image{}) != nil {
		log.Panic("Could not remove data! Seed aborted.")
	} else {
		log.Println("Dropping all tables completed")

		if err := database.Migrator().AutoMigrate(&models.Book{}, &models.Image{}); err != nil {
			log.Panic("Auto migration failed: ", err.Error())
		}

		log.Println("Auto migration succeed")

		books := []models.Book{
			{Title: "Golang is great", Author: "Mr. Great", Year: "2012", Description: "Another golang book", Image: models.Image{Type: "url", Data: "https://go.dev/blog/go-brand/logos.jpg"}},
			{Title: "C++ is greatest", Author: "Mr. C++", Year: "2015", Description: "", Image: models.Image{Type: "base64", Data: "225145254458449946754564644679797846431"}},
			{Title: "C++ is very old", Author: "Mr. Old", Year: "2014", Description: "", Image: models.Image{Type: "url", Data: "https://go.dev/blog/go-brand/logos.jpg"}},
			{Title: "A book without image", Author: "Mr. Image", Year: "2023", Description: ""}}

		for _, book := range books {
			database.Create(&book)
		}

		log.Println("Database successfully sedded!")
	}
}
