package db

import (
	"log"

	"gitlab.com/mobica-workshops/examples/go/gin/book-list/src/models"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func ConnectDB(connectionString string) *gorm.DB {
	db, err := gorm.Open(postgres.Open(connectionString), &gorm.Config{})

	if err != nil {
		log.Fatal("Failed to connect to the Database")
	}

	if err := db.AutoMigrate(&models.Book{}, &models.Image{}); err != nil {
		log.Println("Automigrate error:", err.Error())
	}

	log.Println("Connected Successfully to the Database")

	return db
}

func DisconnectDB(db *gorm.DB) {
	dbSQL, err := db.DB()
	if err != nil {
		panic("Failed to kill connection from database")
	}

	dbSQL.Close()
}
