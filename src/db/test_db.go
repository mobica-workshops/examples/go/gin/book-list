package db

import (
	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func GetTestDB() (*gorm.DB, sqlmock.Sqlmock)  {
	db, mock, _ := sqlmock.New()
	dialector := mysql.New(mysql.Config{
		DSN:                       "sqlmock",
		DriverName:                "mysql",
		Conn:                      db,
		SkipInitializeWithVersion: true,
	})
	gdb, _ := gorm.Open(dialector, &gorm.Config{})

	return gdb, mock
}
