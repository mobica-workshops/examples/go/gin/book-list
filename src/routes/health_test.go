package routes

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestAddHealthRoutesCheckHealth(t *testing.T) {
	router := gin.Default()
	mainRoute := router.Group("/v1")
	service := MockedHealthService{}
	AddHealthRoutes(mainRoute, &service)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/v1/health", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "\"check health\"", w.Body.String())
}
