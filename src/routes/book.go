package routes

import (
	"gitlab.com/mobica-workshops/examples/go/gin/book-list/src/services"

	"github.com/gin-gonic/gin"
)

func AddBookRoutes(rg *gin.RouterGroup, service services.BookService) {
	books := rg.Group("/books")

	books.GET("", service.GetBooks)
	books.POST("", service.CreateBook)
	books.GET("/:id", service.GetBook)
	books.PUT("/:id", service.UpdateBook)
	books.DELETE("/:id", service.DeleteBook)
}
