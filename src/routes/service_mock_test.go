package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type MockedBookService struct {
}

func (s *MockedBookService) CreateBook(c *gin.Context) {
	c.JSON(http.StatusOK, "create book")
}

func (s *MockedBookService) UpdateBook(c *gin.Context) {
	c.JSON(http.StatusOK, "update book")
}

func (s *MockedBookService) DeleteBook(c *gin.Context) {
	c.JSON(http.StatusOK, "delete book")
}

func (s *MockedBookService) GetBooks(c *gin.Context) {
	c.JSON(http.StatusOK, "get books")
}

func (s *MockedBookService) GetBook(c *gin.Context) {
	c.JSON(http.StatusOK, "get book")
}

type MockedHealthService struct {
}

func (s *MockedHealthService) CheckHealth(c *gin.Context) {
	c.JSON(http.StatusOK, "check health")
}
