package routes

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestAddBookRoutesGetBooks(t *testing.T) {
	router := gin.Default()
	mainRoute := router.Group("/v1")
	service := MockedBookService{}
	AddBookRoutes(mainRoute, &service)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/v1/books", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "\"get books\"", w.Body.String())
}

func TestAddBookRoutesGetBook(t *testing.T) {
	router := gin.Default()
	mainRoute := router.Group("/v1")
	service := MockedBookService{}
	AddBookRoutes(mainRoute, &service)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/v1/books/1", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "\"get book\"", w.Body.String())
}

func TestAddBookRoutesCreateBook(t *testing.T) {
	router := gin.Default()
	mainRoute := router.Group("/v1")
	service := MockedBookService{}
	AddBookRoutes(mainRoute, &service)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/v1/books", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "\"create book\"", w.Body.String())
}

func TestAddBookRoutesUpdateBook(t *testing.T) {
	router := gin.Default()
	mainRoute := router.Group("/v1")
	service := MockedBookService{}
	AddBookRoutes(mainRoute, &service)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("PUT", "/v1/books/1", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "\"update book\"", w.Body.String())
}

func TestAddBookRoutesDeleteBook(t *testing.T) {
	router := gin.Default()
	mainRoute := router.Group("/v1")
	service := MockedBookService{}
	AddBookRoutes(mainRoute, &service)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("DELETE", "/v1/books/1", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "\"delete book\"", w.Body.String())
}
