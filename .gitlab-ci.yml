# Version 4.0
stages:
  - configure
  - pre-check
  - build
  - test
  - release

include:
  - project: 'mobica-workshops/templates/ci/gitlab-ci'
    file: '.go-ci-template.yml'
  - project: 'mobica-workshops/templates/ci/gitlab-ci'
    file: '.ansible-ci-template.yml'
  - project: 'mobica-workshops/templates/ci/gitlab-ci'
    file: '.docker-ci-template.yml'
  - project: 'mobica-workshops/templates/ci/gitlab-ci'
    file: '.doc-ci-template.yml'

###############################################
# Configure stage
###############################################
# Generate secrets
configure:
  stage: configure
  extends: .ansible_create_configuration_job_definition

###############################################
# pre-check stage
###############################################
# Run code style and static analysis on the code
run_static_analyse_check:
  extends:
    .go_static_analysis_job_definition
  stage: pre-check
  dependencies:
    - configure

###############################################
# build stage
###############################################
# Build application
build_package:
  extends:
    .go_build_job_definition
  stage: build
  dependencies:
    - configure
    - run_static_analyse_check
  variables:
    APP_NAME: book-list

# Generate documentation
generate_doc:
  variables:
    APP_NAME: book-list
  stage: build
  extends: .doc_package_job_definition
  dependencies:
    - configure

###############################################
# Test stage
###############################################
# Run UT tests with coverage enabled
run_ut_coverage_tests:
  services:
    - name: postgres:14-alpine
      alias: book-list-db
    - name: redis:6-alpine
      alias: redis
  variables:
    APP_NAME: book-list
    POSTGRES_PASSWORD: 2vQU45haQCnDS8sO
    POSTGRES_USER: book-list
    POSTGRES_DB: book-list
  dependencies:
    - configure
    - build_package
  stage: test
  extends:
    .go_run_unit_tests_with_coverage

# Run E2E tests with coverage enabled
run_e2e_coverage_tests:
  services:
    - name: postgres:14-alpine
      alias: book-list-db
    - name: redis:6-alpine
      alias: redis
  variables:
    APP_NAME: book-list
    APP_PORT: 8882
    POSTGRES_PASSWORD: 2vQU45haQCnDS8sO
    POSTGRES_USER: book-list
    POSTGRES_DB: book-list
    E2E_BRANCH: master
    BENCHMARK: "false"
    E2E_REPO_NAME: ex-e2e-api-test-framework
    E2E_REPO_URL: "https://gitlab.com/ldath-core/workshops/core/$E2E_REPO_NAME.git"
    E2E_MARKER: book_list
  dependencies:
    - configure
  stage: test
  extends:
    .go_run_e2e_tests_with_coverage

###############################################
# Release stage
###############################################
# Create image
docker_branch:
  variables:
    DOCKER_FILE: single-stage.Dockerfile
  stage: release
  extends: .gitlab_docker_build_job_definition_kaniko
  dependencies:
    - build_package
  except:
    - master
    - tags
  when: manual

docker_release_latest:
  stage: release
  variables:
    CONTAINER_IMAGE: $CI_REGISTRY_IMAGE:latest
    DOCKER_FILE: single-stage.Dockerfile
  extends: .gitlab_docker_build_job_definition_kaniko
  dependencies:
    - build_package
  only:
    - master
  when: manual

docker_release_tag:
  stage: release
  variables:
    DOCKER_FILE: single-stage.Dockerfile
  extends: .gitlab_docker_build_job_definition_kaniko
  dependencies:
    - build_package
  only:
    - tags

# Generate project html documentation
pages:
  variables:
    APP_NAME: book-list
  stage: release
  extends: .doc_pages_job_definition
  dependencies:
    - generate_doc
