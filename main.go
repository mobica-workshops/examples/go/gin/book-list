package main

import (
	"gitlab.com/mobica-workshops/examples/go/gin/book-list/cmd"
)

func main() {
	cmd.Execute()
}
