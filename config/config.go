package config

import (
	"log"

	"github.com/spf13/viper"
)

type DBConnectionURL struct {
	Url string `mapstructure:"url"`
}

type ServerConfig struct {
	Host string `mapstructure:"host"`
	Port string `mapstructure:"port"`
}

type Config struct {
	DBConnection DBConnectionURL `mapstructure:"postgres"`
	Server       ServerConfig    `mapstructure:"server"`
}

func GetConfiguration() (*Config, error) {
	var config *Config

	if err := viper.Unmarshal(&config); err != nil {
		log.Fatal("Parsing configuration failed, err:", err)

		return config, err
	}

	return config, nil
}
